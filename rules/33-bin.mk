#
# Produce a binary executable.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/33-bin.mk
# @license
#

# Set the tools:
OBJCOPY ?= objcopy
SIZE    ?= size

ifeq ($(TYPE),bin)
ifneq ($(SOURCES),)

# Set the output file names:
OUT ?= $(NAME)$(OUT_EXT)

all:: $(OUT) $(BIN) $(HEX) $(UFF)

$(OUT):: $(OBJECTS) $(EXTRA_DEPS)
	$(ECHO) 'LD    $(CWD)/$@'
	$(CROSS)$(TOOLCHAIN_FRONTEND) $(_LDFLAGS) -o $@ $^ $(_LIBS) $(_LDS)

# Conditionally add .text size and CRC:
ifneq ($(APPEND_CRC),)
$(OUT):: $(OBJECTS) $(EXTRA_DEPS)
	$(ECHO) 'CRC   $(CWD)/$@'
	$(CROSS)$(TOOLCHAIN_FRONTEND) $(_LDFLAGS) -o $@ $^ $(_LIBS) $(_LDS) `$(BUILDDIR)/tools/crc.py $@`
endif

# Conditionally show the binary size:
ifneq ($(SHOW_SIZE),)
$(OUT):: $(OBJECTS) $(EXTRA_DEPS)
	$(CROSS)$(SIZE) $@
endif

$(BIN):: $(OUT)
	$(ECHO) 'BIN   $(CWD)/$@'
	$(CROSS)$(OBJCOPY) -O binary $< $@

$(HEX):: $(OUT)
	$(ECHO) 'HEX   $(CWD)/$@'
	$(CROSS)$(OBJCOPY) -O ihex $< $@

$(UFF):: $(HEX)
	$(ECHO) 'UFF   $(CWD)/$@'
	$(BUILDDIR)/tools/uff.py $< $@

# Add artefacts to the purge list:
PURGE += $(OUT) $(BIN) $(HEX) $(UFF)

endif
endif

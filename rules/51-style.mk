#
# Beautify the code.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/51-style.mk
# @license
#

# Set the tools:
UNCRUSTIFY ?= uncrustify

# Set default source files:
UNCRUSTIFY_SOURCES ?= $(SOURCES) $(wildcard *.h *.hpp)

# Exclude the specified source files and add extra ones:
UNCRUSTIFY_SOURCES := $(filter-out $(EXCLUDE_UNCRUSTIFY_SOURCES),$(UNCRUSTIFY_SOURCES)) $(EXTRA_UNCRUSTIFY_SOURCES)

# Set the default flags:
UNCRUSTIFY_FLAGS += -c $(BUILDDIR)/configs/uncrustify.cfg

# Check source files:
ifneq ($(strip $(UNCRUSTIFY_SOURCES)),)

style-check:: $(UNCRUSTIFY_SOURCES)
	$(UNCRUSTIFY) $(UNCRUSTIFY_FLAGS) --check -q $^

style-fix:: $(UNCRUSTIFY_SOURCES)
	$(UNCRUSTIFY) $(UNCRUSTIFY_FLAGS) --replace --no-backup --mtime $^

endif

style-check::
style-fix::
.PHONY:: style-check style-fix

#
# Preliminary setup.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/10-preliminaries.mk
# @license
#

# Set NAME to the name of the current directory:
NAME ?= $(notdir $(CURDIR))

# Set CWD to the relative path to TOPDIR:
ifeq ($(TOPDIR),$(CURDIR))
  CWD := .
else
  CWD := $(subst $(TOPDIR)/,,$(CURDIR))
endif

# Select verbose or quiet builds:
VERBOSE	?= no
ifeq ($(VERBOSE),no)
  ECHO       = @echo
  MAKEFLAGS += --no-print-directory -s
  export MAKEFLAGS
else
  ECHO = @true
endif

# Disable builtin rules and variables:
MAKEFLAGS += -r -R
.SUFFIXES:

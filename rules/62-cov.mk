#
# Generate code coverage report.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/62-cov.mk
# @license
#

# Set the tools:
GCOV  ?= gcov
GCOVR ?= gcovr

GCOVR_OUTPUT ?= doc/coverage

# Set default gcovr flags:
GCOVR_FLAGS += --gcov-executable=$(CROSS)$(GCOV) --html --html-details
GCOVR_FLAGS += --output=$(GCOVR_OUTPUT)/index.html

gen-cov::
	$(ECHO) 'GEN   coverage'
	mkdir -p $(GCOVR_OUTPUT)
	$(GCOVR) $(GCOVR_FLAGS)

# Add artefacts to the purge list:
PURGE += $(GCOVR_OUTPUT)

.PHONY:: gen-cov

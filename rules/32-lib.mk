#
# Produce a dynamic library.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/32-lib.mk
# @license
#

ifeq ($(TYPE),lib)
ifneq ($(SOURCES),)

# Set the output file name:
LIB ?= lib$(NAME).so

# Dynamic libraries must be built from position-independent code:
ifeq ($(TOOLCHAIN_LANGUAGE),c)
  _CFLAGS   += -fpic
endif
ifeq ($(TOOLCHAIN_LANGUAGE),c++)
  _CXXFLAGS += -fPIC
endif

all:: $(LIB)

$(LIB):: $(OBJECTS) $(EXTRA_DEPS)
	$(ECHO) 'LD    $(CWD)/$@'
	$(CROSS)$(TOOLCHAIN_FRONTEND) -shared $(_LDFLAGS) -o $@ $^ $(_LIBS)

# Add artefacts to the purge list:
PURGE += $(LIB)

endif
endif

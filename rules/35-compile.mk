#
# Compile source files.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/35-compile.mk
# @license
#

ifneq ($(SOURCES),)

# Extra compiler flags to generate a dependency files:
_DEPFLAGS = -MMD -MF $@.d

# Compile C files:
%.o: %.c $(DEPS)
	$(ECHO) 'CC    $(CWD)/$@'
	$(CROSS)$(CC) $(_CFLAGS) $(_DEPFLAGS) -c -o $@ $<

# Compile C++ files:
%.o: %.cpp $(DEPS)
	$(ECHO) 'CXX   $(CWD)/$@'
	$(CROSS)$(CXX) $(_CXXFLAGS) $(_DEPFLAGS) -c -o $@ $<

# Assemble .S files:
%.o: %.[sS] $(DEPS)
	$(ECHO) 'AS    $(CWD)/$@'
	$(CROSS)$(CC) $(_CFLAGS) $(_DEPFLAGS) -D__ASSEMBLY__ -c -o $@ $<

# Include the build dependencies:
-include $(foreach obj,$(OBJECTS),$(obj).d)

# Add compile artefacts to the purge list:
PURGE += $(OBJECTS) $(addsuffix .d,$(OBJECTS))

# Add coverage artefacts to the purge list:
PURGE += $(wildcard *.gcda *.gcno)

endif

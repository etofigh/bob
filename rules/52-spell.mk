#
# Check the spelling.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/52-spell.mk
# @license
#

# Set the tools:
CODESPELL ?= codespell

# Set default source files:
CODESPELL_SOURCES ?= $(SOURCES) $(wildcard *.h *.hpp)

# Exclude the specified source files and add extra ones:
CODESPELL_SOURCES := $(filter-out $(EXCLUDE_CODESPELL_SOURCES),$(CODESPELL_SOURCES)) $(EXTRA_CODESPELL_SOURCES)

# Set the default flags:
ifneq ($(wildcard $(TOPDIR)/.spellignore),)
  CODESPELL_FLAGS += -I $(TOPDIR)/.spellignore
endif

# Check source files:
ifneq ($(strip $(CODESPELL_SOURCES)),)

spell-check:: $(CODESPELL_SOURCES)
	$(CODESPELL) $(CODESPELL_FLAGS) $^

spell-fix:: $(CODESPELL_SOURCES)
	$(CODESPELL) $(CODESPELL_FLAGS) --write-changes $^

endif

spell-check::
spell-fix::
.PHONY:: spell-check spell-fix

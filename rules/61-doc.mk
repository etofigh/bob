#
# Generate code documentation.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/60-doc.mk
# @license
#

# Set the tools:
DOXYGEN ?= doxygen

# Set default doxygen flags:
DOXYGEN_FLAGS += $(BUILDDIR)/configs/Doxyfile

gen-doc::
	$(ECHO) 'GEN   documentation'
	$(DOXYGEN) $(DOXYGEN_FLAGS)

# Add artefacts to the purge list:
PURGE += doc/doxygen

.PHONY:: gen-doc

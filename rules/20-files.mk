#
# Find source and object files.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/20-files.mk
# @license
#

# Set default source files:
SOURCES  ?= $(wildcard *.[csS] *.cpp)

# Exclude the specified source files and add extra ones:
SOURCES  := $(filter-out $(EXCLUDE_SOURCES),$(SOURCES)) $(EXTRA_SOURCES)

# Set default include directories:
INCLUDES ?= $(TOPDIR)/include

# Exclude the specified include directories and add extra ones:
INCLUDES := $(filter-out $(EXCLUDE_INCLUDES),$(INCLUDES)) $(EXTRA_INCLUDES)

# Derive objects names from source files:
OBJECTS  ?= $(foreach src,$(SOURCES),             \
	 $(patsubst %.S,%.o,$(filter %.S,$(src))) \
	 $(patsubst %.s,%.o,$(filter %.s,$(src))) \
	 $(patsubst %.c,%.o,$(filter %.c,$(src))) \
	 $(patsubst %.cpp,%.o,$(filter %.cpp,$(src))))

# Exclude the specified objects and add extra ones:
OBJECTS  := $(filter-out $(EXCLUDE_OBJECTS),$(OBJECTS)) $(EXTRA_OBJECTS)

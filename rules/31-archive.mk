#
# Produce a static library.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/31-archive.mk
# @license
#

# Set the tools:
AR ?= ar

ifeq ($(TYPE),archive)
ifneq ($(SOURCES),)

# Set the output file name:
ARCHIVE ?= lib$(NAME).a

all:: $(ARCHIVE)

$(ARCHIVE):: $(OBJECTS) $(EXTRA_DEPS)
	$(ECHO) 'AR    $(CWD)/$@'
	$(CROSS)$(AR) rcsT $@ $^

# Add artefacts to the purge list:
PURGE += $(ARCHIVE)

endif
endif

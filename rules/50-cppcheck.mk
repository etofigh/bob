#
# Run cppcheck static code analyser.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/50-cppcheck.mk
# @license
#

# Set the tools:
CPPCHECK ?= cppcheck

# Set default source files:
CPPCHECK_SOURCES ?= $(SOURCES)

# Exclude the specified source files and add extra ones:
CPPCHECK_SOURCES := $(filter-out $(EXCLUDE_CPPCHECK_SOURCES),$(CPPCHECK_SOURCES)) $(EXTRA_CPPCHECK_SOURCES)

ifneq ($(CPPCHECK_SOURCES),)

# Split the C and C++ source files:
CPPCHECK_C_SOURCES   = $(filter %.c,$(CPPCHECK_SOURCES))
CPPCHECK_CXX_SOURCES = $(filter %.cpp,$(CPPCHECK_SOURCES))

# Set the list of extra checks to enable:
CPPCHECK_EXTRA_CHECKS ?= warning,style,performance,portability,information,missingInclude

# Set the common options to give to cppcheck for both C and C++ sources:
CPPCHECK_COMMON_OPTS := $(addprefix --enable=,$(CPPCHECK_EXTRA_CHECKS))             \
                        --force --error-exitcode=1 --inline-suppr -j $(shell nproc) \
			--suppress=unmatchedSuppression $(CPPCHECK_EXTRA_FLAGS)     \
			-I$(INCLUDES)

# Check C source files:
ifneq ($(CPPCHECK_C_SOURCES),)

cppcheck:: $(CPPCHECK_C_SOURCES)
	$(ECHO) 'CPPCHK $(CWD)'
	$(CPPCHECK) $(CPPCHECK_COMMON_OPTS) $^

endif


# Check C++ source files:
ifneq ($(CPPCHECK_CXX_SOURCES),)

cppcheck:: $(CPPCHECK_CXX_SOURCES)
	$(ECHO) 'CPPCHK $(CWD)'
	$(CPPCHECK) $(CPPCHECK_COMMON_OPTS) $^
endif

endif

cppcheck::
.PHONY:: cppcheck

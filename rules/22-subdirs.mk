#
# Recurse into sub-directories.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/22-subdirs.mk
# @license
#

# Set default sub-directories:
SUBDIRS    ?= $(patsubst %/,%,$(wildcard */))

# Add extra sub-directories:
SUBDIRS    := $(filter-out $(EXCLUDE),$(SUBDIRS)) $(EXTRA_SUBDIRS)

# Set default recursive targets:
RECTARGETS ?= all clean install cppcheck style-check style-fix spell-check spell-fix

# Add extra recursive targets:
RECTARGETS := $(filter-out $(EXCLUDE_RECTARGETS),$(RECTARGETS)) $(EXTRA_RECTARGETS)

# Recursively run make in all SUBDIRS:
ifneq ($(SUBDIRS),)
ifneq ($(RECTARGETS),)
$(RECTARGETS)::
	$(MAKE) -f $(TOPDIR)/Makefile _recurse _target=$@

_recurse: | $(SUBDIRS)

.PHONY:: _recurse $(RECTARGETS)
endif

$(SUBDIRS)::
	$(MAKE) -f $(TOPDIR)/Makefile -C $@ $(_target)
endif

#
# Produce object files from binary blobs.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/34-binobj.mk
# @license
#

LD ?= ld

ifdef BINOBJ

OBJECTS += $(addsuffix .o,$(basename $(BINOBJ)))

all:: $(OBJECTS)

$(OBJECTS):: %.o: $(BINOBJ)
	$(ECHO) 'LD    $(CWD)/$@'
	$(CROSS)$(LD) --relocatable --format=binary -o $@ \
	  $(foreach f,$(BINOBJ),$(if $(filter $(basename $(f)),$*),$(f)))

# Add link artefacts to the purge list:
PURGE += $(OBJECTS)

endif

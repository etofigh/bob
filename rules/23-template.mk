#
# Generate files from templates.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/23-template.mk
# @license
#

# Set default template files:
TEMPLATES ?= $(wildcard *.tpl)

ifneq ($(TEMPLATES),)

%: %.tpl
	$(ECHO) 'GEN   $(CWD)/$(notdir $@)'
	envsubst < $< > $@

# Add generated files to the purge list:
PURGE += $(patsubst %.tpl,%,$(TEMPLATES))

endif

#
# Generate change log.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/60-change.mk
# @license
#

# Set default changelog location:
CHANGELOG_FILE ?= doc/changelog.txt

gen-change::
	$(ECHO) 'GEN   $(CWD)/$(CHANGELOG_FILE)'
	$(BUILDDIR)/tools/changelog.py $(CHANGELOG_FILE)

# Add file to the purge list:
PURGE += $(CHANGELOG_FILE)

.PHONY:: gen-change

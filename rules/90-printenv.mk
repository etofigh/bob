#
# Print build variables to ease interaction with scripts.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/90-printenv.mk
# @license
#

# Print the build variables applicable in the current directory:
printenv:
	$(ECHO) CROSS=\"$(strip $(CROSS))\"
	$(ECHO) CFLAGS=\"$(strip $(_CFLAGS))\"
	$(ECHO) LDFLAGS=\"$(strip $(_LDFLAGS))\"
	$(ECHO) LIBS=\"$(strip $(_LIBS))\"
	$(ECHO) LDS=\"$(strip $(_LDS))\"

.PHONY:: printenv

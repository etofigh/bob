#
# Clean build artefacts.
#
# @author  Ehsan Tofigh
# @project bob
# @file    Clean build artefacts
# @license
#

# Set the tools:
RM := rm -rf --

# Exclude the specified files:
PURGE := $(filter-out $(EXCLUDE_PURGE),$(PURGE))

ifneq ($(PURGE),)

# Create a temporary file to pass the purge list to the RM command to avoid the
# command line length limit:
_PURGE = .purge
PURGE += $(_PURGE)

clean::
	$(ECHO) 'RM    $(CWD)'
	$(file >$(_PURGE),$(PURGE))
	xargs --arg-file=$(_PURGE) $(RM)

endif

clean::
.PHONY:: clean

#
# Install files to specified locations.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/41-install.mk
# @license
#

ifneq ($(INSTALL),)

# Set the top level install directory:
INSTALL_PREFIX   ?= $(TOPDIR)/bin
INSTALL_DEST_DIR := $(INSTALL_PREFIX)/$(INSTALL_DIR)

# Define the install target:
install:: $(addprefix $(INSTALL_DEST_DIR)/,$(INSTALL))

$(INSTALL_DEST_DIR)/%: %
	mkdir -v -p $(dir $@)
	cp -r -v -T $^ $@

endif

install::
.PHONY:: install

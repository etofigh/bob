#
# Flash executable onto the target.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/70-flash.mk
# @license
#

# Set the tools:
JLINK ?= JLinkExe

ifdef HEX

# Set the default flags to connect to target:
JLINK_FLAGS += -device $(DEVICE) -if SWD -speed 4000 -autoconnect 1

flash:: $(HEX)
	$(ECHO) 'FLASH $(CWD)/$<'
	envsubst < $(BUILDDIR)/configs/flash.jlink > $(TOPDIR)/flash.jlink
	$(JLINK) $(JLINK_FLAGS) -CommanderScript $(TOPDIR)/flash.jlink

# Add generated files to the purge list:
PURGE += $(TOPDIR)/flash.jlink

endif

flash::
.PHONY:: flash

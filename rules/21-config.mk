#
# Set default toolchain and compiler flags.
#
# @author  Ehsan Tofigh
# @project bob
# @file    rules/21-config.mk
# @license
#

# Add include directories:
GENERAL_CFLAGS      += $(addprefix -I,$(INCLUDES))
GENERAL_CFLAGS      += $(addprefix -isystem,$(SYSTEM_INCLUDES))

# Set generic (non target-specific) compile flags:
GENERAL_CFLAGS      += -Wall -Wextra
GENERAL_CFLAGS      += -Wdouble-promotion
GENERAL_CFLAGS      += -Wfloat-equal
GENERAL_CFLAGS      += -Wredundant-decls
GENERAL_CFLAGS      += -Wshadow
GENERAL_CFLAGS      += -Wswitch-default
GENERAL_CFLAGS      += -Wno-packed-bitfield-compat
GENERAL_CFLAGS      += -funsigned-char
GENERAL_CFLAGS      += -O2
GENERAL_CXXFLAGS     = $(GENERAL_CFLAGS)

# Add extra compiler flags to enable coverage analysis:
ifneq ($(COVERAGE),)
  GENERAL_CFLAGS    += --coverage
  GENERAL_LDFLAGS   += --coverage
  DEBUG             := 1
endif

# Add extra compiler flags for debug mode:
ifneq ($(DEBUG),)
  GENERAL_CFLAGS    += -ggdb3 -O0 -DDEBUG
  GENERAL_CXXFLAGS  += -ggdb3 -O0
  GENERAL_LDFLAGS   += -ggdb3
endif

# Combine all compiler flags:
_CFLAGS              = $(GENERAL_CFLAGS)   $(TARGET_CFLAGS)   $(CFLAGS)
_CXXFLAGS            = $(GENERAL_CXXFLAGS) $(TARGET_CXXFLAGS) $(CXXFLAGS)
_LDFLAGS             = $(GENERAL_LDFLAGS)  $(TARGET_LDFLAGS)  $(LDFLAGS)
_LIBS                = $(GENERAL_LIBS)     $(TARGET_LIBS)     $(LIBS)
_LDS                 = $(GENERAL_LDS)      $(TARGET_LDS)      $(LDS)

# Determine the source language and set the appropriate toolchain frontend:
CC  ?= gcc
CXX ?= g++

ifneq ($(filter %.cpp,$(SOURCES)),)
  TOOLCHAIN_LANGUAGE = c++
  TOOLCHAIN_FRONTEND = $(CXX)
  TOOLCHAIN_FLAGS    = $(_CXXFLAGS)
else
  TOOLCHAIN_LANGUAGE = c
  TOOLCHAIN_FRONTEND = $(CC)
  TOOLCHAIN_FLAGS    = $(_CFLAGS)
endif

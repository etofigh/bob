# Bob (the Builder)

Bob the Builder is a set of Make rules and various configuration files to
automate checking, building and testing my projects.

To build the code for the host platform:
```shell
make
```


To build the code for a specific target:
```shell
make TARGET=arm
```


To build the code for debugging:
```shell
make DEBUG=1
```


To generate the code coverage report run (after running the tests):
```shell
make gen-coverage
```


To generate change log from the git history:
```shell
make gen-change
```


To check the code for styling issues:
```shell
make style-check
```


To fix the styling issues:
```shell
make style-fix
```

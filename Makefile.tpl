#
# Main Makefile.
#
# @author  Ehsan Tofigh
# @project bob
# @file    Makefile.tpl
# @license
#

# Ensure that 'all' is the default/implicit build target:
all::
TARGET ?= host

# Set TOPDIR to the root of the source tree:
TOPDIR ?= $(CURDIR)
export TOPDIR

# Set BUILDDIR to the root of the build-system:
BUILDDIR ?= $(dir $(realpath $(TOPDIR)/$(shell readlink $(TOPDIR)/Makefile)))

# Include the project Makefile from the TOPDIR (applied to all directories):
-include $(TOPDIR)/project.mk

# Include the directory Makefile (applied to current directory):
-include local.mk

# Include target-specific Makefile:
-include $(BUILDDIR)/target/$(TARGET).mk

# Include all post Makefile rules (applied after local.mk):
-include $(sort $(wildcard $(BUILDDIR)/rules/*))

.PHONY:: all

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# USB File Format conversion tool.
#
# @author  Ehsan Tofigh
# @project bob
# @file    tools/uff.py
# @license
#

import argparse
import intelhex
import os
import struct
import sys

sof = int.from_bytes(b'UFF1', byteorder='little')
eof = int.from_bytes(b'UFF2', byteorder='little')
chunk_size = 492

def get_args():
	"""Handle command line arguments."""

	# Argument parser.
	parser = argparse.ArgumentParser()

	parser.add_argument('input',        type=str,            help='path to the input hex/bin file')
	parser.add_argument('output',       type=str,            help='path to the output uff file')
	parser.add_argument('-a', '--addr', type=int, default=0, help='address offset in case of a binary file')

	# Parse arguments.
	return parser.parse_args()


if __name__ == "__main__":
	"""Entry point of the script."""

	# Get the command line arguments.
	args = get_args()

	name, ext = os.path.splitext(args.input)

	# Handle both hex and binary files.
	if ext == '.hex':
		ih = intelhex.IntelHex(args.input)
		segments = ih.segments()
		read_fn  = ih.tobinarray
	elif ext == '.bin':
		with open(args.input, 'rb') as f:
			bi = f.read()

		segments = [(args.addr, len(bi))]
		read_fn  = lambda start, size : bi[addr:addr + size]
	else:
		sys.exit('Invalid input file')

	# Convert the file into UFF.
	uff = bytes()
	for start, end in segments:
		total = end - start
		crc, = struct.unpack('<I', bytes(read_fn(start=end-4, size=4)))
		for addr in range(start, end, chunk_size):
			size = chunk_size if end - addr > chunk_size else end - addr
			chunk = struct.pack(F'<IIII{chunk_size}sI', sof, addr, total, crc, bytes(read_fn(start=addr, size=size)), eof)
			uff += chunk

	# Write the UFF file to disk.
	with open(args.output, 'wb') as f:
		f.write(uff)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Tool to calculate the binary size and CRC of an ELF file.
#
# @author  Ehsan Tofigh
# @project bob
# @file    tools/crc.py
# @license
#

import argparse
import crcmod
import subprocess
import struct

# CRC function used by the MCU.
crc = crcmod.mkCrcFun(0x104C11DB7, rev=False)

def get_args():
	"""Handle command line arguments."""

	# Argument parser.
	parser = argparse.ArgumentParser()

	parser.add_argument('input', type=str, help='path to the input elf file')

	# Parse arguments.
	return parser.parse_args()


if __name__ == "__main__":
	"""Entry point of the script."""

	# Get the command line arguments.
	args = get_args()

	# Run the objcopy command to get the contents of the .text section:
	bin = subprocess.check_output(['arm-none-eabi-objcopy', '-O' , 'binary',
				      args.input, '/dev/stdout'])

	# Pack the size into the binary for correct CRC calculation.
	buf = bytearray(bin)
	struct.pack_into('<I', buf, 512, len(buf))

	print(F'-Wl,--defsym=TEXT_SIZE={len(buf)},--defsym=TEXT_CRC=0x{crc(buf[:-4]):08X}')

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This script generates a changelog based on the git history.
#
# @author  Ehsan Tofigh
# @project bob
# @file    tools/changelog.py
# @license
#

import argparse
import os
import subprocess

def get_args():
	"""
	Handle command line arguments.

	Returns:
		opts (dict): Dictionary containing all command line arguments.
	"""

	# Argument parser.
	parser = argparse.ArgumentParser()
	parser.add_argument('path', type=str, help="path of the output file")

	# Parse arguments.
	args = parser.parse_args()

	# Return arguments.
	return args

if __name__ == "__main__":
	"""
	Main entry point of the changelog tool.

	Returns:
		exitcode (bool): The exit code of the program.
	"""

	# Get command line arguments.
	args = get_args()

	# Get all commits.
	cmd = 'git log --pretty=format:%H|%s'
	res = subprocess.run(cmd.split(), check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	commits = [tag.split('|') for tag in res.stdout.decode("utf-8").split('\n')]

	# Get parent commits.
	cmd = 'git log --first-parent --pretty=format:%H'
	res = subprocess.run(cmd.split(), check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	parents = res.stdout.decode("utf-8").split('\n')

	# Get all tags.
	cmd = 'git for-each-ref --format=%(*objectname)|%(refname)|%(taggerdate)|%(objecttype) --sort=-taggerdate refs/tags'
	res = subprocess.run(cmd.split(), check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	tags = [tag.split('|') for tag in res.stdout.decode("utf-8").split('\n') if tag != '']

	# Build the changelog.
	changelog = ''
	for commit in commits:
		for tag in tags:
			if commit[0] == tag[0]:
				changelog += "\n* Release {} - {}\n".format(tag[1].split('refs/tags/')[1], tag[2])

		changelog += "      - " if commit[0] in parents else "    "
		changelog += commit[1] + "\n"

	# Write the changelog to a file.
	os.makedirs(os.path.dirname(args.path), exist_ok=True)
	with open(args.path, 'w') as f:
		f.write(changelog)

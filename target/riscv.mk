#
# Toolchain configuration for RISC-V targets.
#
# @author  Ehsan Tofigh
# @project bob
# @file    target/riscv.mk
# @license
#

# Set target cross-compiler:
CROSS = riscv-none-elf-

# Define extra compiler flags:
TARGET_CFLAGS  += -march=rv32imc -std=c11 -g -ffunction-sections -fdata-sections
TARGET_LDFLAGS += -nostartfiles --specs=nano.specs --specs=nosys.specs
TARGET_LDFLAGS += -Wl,--gc-sections
TARGET_LDS     += -march=rv32imc -L $(TOPDIR) -T $(LD_FILE)

# Set the binary extension:
OUT_EXT := .elf

# Produce a BIN file for the target:
BIN ?= $(NAME).bin

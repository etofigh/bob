#
# Toolchain configuration for host targets.
#
# @author  Ehsan Tofigh
# @project bob
# @file    target/host.mk
# @license
#

# Set target cross-compiler:
CROSS ?=

# Set target compiler flags:
TARGET_CFLAGS   += $(shell getconf LFS64_CFLAGS) -std=c11
TARGET_CXXFLAGS += $(shell getconf LFS64_CFLAGS) -std=c++17
TARGET_LDFLAGS  += $(shell getconf LFS64_LDFLAGS)
TARGET_LIBS     +=

#
# Toolchain configuration for ARM targets.
#
# @author  Ehsan Tofigh
# @project bob
# @file    target/arm.mk
# @license
#

# Set target cross-compiler:
CROSS = arm-none-eabi-

# Define extra compiler flags:
TARGET_CFLAGS  += -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -g -ffunction-sections
TARGET_LDFLAGS += -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -nostartfiles --specs=nano.specs
TARGET_LDFLAGS += -Wl,--gc-sections,--undefined=lcx_build,--defsym=TEXT_SIZE=0,--defsym=TEXT_CRC=0
TARGET_LDS     += -L $(TOPDIR) -T $(LD_FILE)

# Set the output binary extension:
OUT_EXT := .elf

# Produce HEX and UFF files for ARM targets:
HEX ?= $(NAME).hex
UFF ?= $(NAME).uff

# Append CRC:
APPEND_CRC := 1

# Coverage analysis requires semihosting on ARM targets:
ifneq ($(COVERAGE),)
  SEMIHOST := 1
endif

# Add extra compiler flags to enable semihosting:
ifneq ($(SEMIHOST),)
  TARGET_CFLAGS  += -DSEMIHOST
  TARGET_LIBS    += -lc -lrdimon
  TARGET_LDFLAGS += --specs=rdimon.specs
  EXTRA_SOURCES  += $(TOPDIR)/bob/target/arm.c
endif

/**
 * Routine for ARM Cortex-M microcontrollers to initialise semihosting.
 *
 * @author  Ehsan Tofigh
 * @project bob
 * @file    target/arm.c
 * @license
 */

/**
 * Executed before entering the main routine.
 */
void pre_main (void)
{
	/* Initialise the GDB monitor when semihosting is enabled: */
	extern void initialise_monitor_handles(void);
	initialise_monitor_handles();
}

#
# Uncrustify configuration file.
#
# @author  Ehsan Tofigh
# @project bob
# @file    configs/uncrustify.cfg
# @license
#

#---------------------------------------------------------------------------
# Indentation
#---------------------------------------------------------------------------
indent_with_tabs                = 2             # 1=indent to level only, 2=indent with tabs
input_tab_size                  = 8             # original tab size
output_tab_size                 = 8             # new tab size
indent_columns                  = output_tab_size
code_width                      = 120           # Break lines over 100 chars
ls_code_width                   = true          # Split lines as close to code_width as possible
indent_label                    = 1             # Labels are always column 1
indent_member                   = 4

#---------------------------------------------------------------------------
# Inter-symbol newlines
#---------------------------------------------------------------------------
nl_enum_brace                   = remove        # "enum {" vs "enum \n {"
nl_union_brace                  = remove        # "union {" vs "union \n {"
nl_struct_brace                 = remove        # "struct {" vs "struct \n {"
nl_do_brace                     = remove        # "do {" vs "do \n {"
nl_if_brace                     = remove        # "if () {" vs "if () \n {"
nl_for_brace                    = remove        # "for () {" vs "for () \n {"
nl_else_brace                   = remove        # "else {" vs "else \n {"
nl_while_brace                  = remove        # "while () {" vs "while () \n {"
nl_switch_brace                 = remove        # "switch () {" vs "switch () \n {"
nl_brace_while                  = remove        # "} while" vs "} \n while" - cuddle while
nl_brace_else                   = remove        # "} else" vs "} \n else" - cuddle else
nl_func_var_def_blk             = 1             # New line after variable definition block
nl_fcall_brace                  = remove        # "list_for_each() {" vs "list_for_each()\n{"
nl_fdef_brace                   = force         # "int foo() {" vs "int foo()\n{"
nl_after_return                 = true          # Always a newline after a return
nl_after_semicolon              = true          # Always a newline after a ; except within for statement
nl_after_func_body              = 2

#---------------------------------------------------------------------------
# Source code modifications
#---------------------------------------------------------------------------
mod_paren_on_return             = remove        # "return 1;" vs "return (1);"

#---------------------------------------------------------------------------
# Inter-character spacing options
#---------------------------------------------------------------------------
sp_return_paren                 = force         # "return (1);" vs "return(1);"
sp_sizeof_paren                 = force         # "sizeof (int)" vs "sizeof(int)"
sp_before_sparen                = force         # "if (" vs "if("
sp_after_sparen                 = force         # "if () {" vs "if (){"
sp_after_cast                   = force         # "(int) a" vs "(int)a"
sp_inside_braces                = force         # "{ 1 }" vs "{1}"
sp_inside_braces_struct         = force         # "{ 1 }" vs "{1}"
sp_inside_braces_enum           = force         # "{ 1 }" vs "{1}"
sp_arith                        = force
sp_bool                         = force
sp_compare                      = force
sp_assign                       = force
sp_assign_default               = force
sp_before_assign                = force
sp_after_assign                 = force
sp_func_def_paren               = force         # "int foo (){" vs "int foo(){"
sp_func_call_paren              = remove        # "foo (" vs "foo("
sp_func_proto_paren             = force         # "int foo ();" vs "int foo();"
sp_paren_paren                  = remove        # Remove space between nested parentheses
sp_inside_paren                 = remove
sp_else_brace                   = force         # force space between else and {
sp_brace_else                   = force         # force space between } else
sp_after_comma                  = force         # Always a space after a comma
sp_before_comma                 = remove        # Never a space before a comma
sp_endif_cmt                    = force

#---------------------------------------------------------------------------
# Alignment
#---------------------------------------------------------------------------
align_mix_var_proto             = false
align_with_tabs                 = false         # Do not use tabs to align
align_on_tabstop                = false         # Do no align on tabstops
align_keep_tabs                 = false         # Remove non-indenting tabs
align_keep_extra_space          = false         # Remove space not used for indenting
align_struct_init_span          = 3             # align stuff in a structure init '= { }'
align_var_struct_span           = 1             # Lines considered for aligning structure members
align_var_struct_thresh         = 24            # Max width considered for aligning structure members
align_var_struct_gap            = 1
align_var_def_star_style        = 2             # Dangling * in variable alignment
align_var_def_amp_style         = 2             # Dangling & in variable alignment
align_right_cmt_span            = 3
align_var_def_span              = 1             # Lines considered for aligning variable definitions 'var ='
align_var_def_thresh            = 12            # Max width considered for aligning variable definitions 'var ='
align_var_def_gap               = 1
align_var_def_attribute         = true
align_var_def_colon             = true
align_var_def_inline            = true
align_assign_span               = 1             # Lines considered for aligning assignments '= 0;'
align_assign_thresh             = 8             # Max width considered for aligning assignments '= 0;'
align_enum_equ_span             = 1
align_enum_equ_thresh           = 16
align_func_proto_span           = 1
align_func_proto_gap            = 1
align_func_params               = true
align_same_func_call_params     = true

#---------------------------------------------------------------------------
# New Lines
#---------------------------------------------------------------------------
nl_start_of_file                = remove        # Remove empty newlines at top of file
nl_end_of_file                  = force         # Force a newline at bottom of file
nl_end_of_file_min              = 1
nl_after_label_colon            = true          # Force a new line after a label colon
nl_before_block_comment         = 1             # Force 1 new line before a block comment unless after a brace
nl_max                          = 2             # No more than 1 blank line anywhere
nl_multi_line_define            = false         # No forced newline after a macro name
nl_squeeze_ifdef                = true          # Remove blanks before and after #ifixx/#elxx/#endif
nl_squeeze_ifdef_top_level      = false         # Don't do that at top level
nl_func_type_name               = remove        # No newline between return type and function name
nl_func_proto_type_name         = remove
nl_func_var_def_blk             = 1             # Always a blank line after a variable block
nl_var_def_blk_start            = 1             # Always a blank line before a variable block
nl_var_def_blk_end              = 1             # Always a blank line after a variable block

#---------------------------------------------------------------------------
# Commas
#---------------------------------------------------------------------------
pos_comma                       = trail         # Commas left on the line above, if split
pos_enum_comma                  = trail         # Commas on the line above, in enums

#---------------------------------------------------------------------------
# Pointers and references
#---------------------------------------------------------------------------
sp_before_ptr_star              = force         # Add a space before pointer star '*'
sp_before_unnamed_ptr_star      = ignore        # No special cases for pointers
sp_between_ptr_star             = remove        # Remove space between pointer stars '*'
sp_after_ptr_star               = remove        # Remove space after pointer star '*', if followed by a word.
sp_after_ptr_star_qualifier     = remove        # Remove space after pointer star '*', if followed by a qualifier.
sp_after_ptr_star_func          = remove        # Remove space after a pointer star '*', if followed by a func proto/def.
sp_ptr_star_paren               = force         # Add a space after a pointer star '*', if followed by an open paren (function types).
sp_before_ptr_star_func         = force         # Add a space before a pointer star '*', if followed by a func proto/def.
sp_before_byref                 = force         # Add a space before a reference sign '&'
sp_before_unnamed_byref         = ignore        # No special cases for references
sp_after_byref                  = remove        # Remove space after reference sign '&', if followed by a word.
sp_after_byref_func             = remove        # Remove space after a reference sign '&', if followed by a func proto/def.
sp_before_byref_func            = force         # Add a space before a reference sign '&', if followed by a func proto/def.

#---------------------------------------------------------------------------
# Comments
#---------------------------------------------------------------------------
cmt_cpp_to_c                    = false         # Convert cpp comments to C style
cmt_c_nl_end                    = true          # Add a newline before the closing of comments
cmt_star_cont                   = true          # Add a star for each line of the comment
cmt_sp_before_star_cont         = 0             # No additional space before each star
cmt_sp_after_star_cont          = 1             # 1 space after each star
cmt_multi_check_last            = false         # Do not remove trailing space on multiline comments

#---------------------------------------------------------------------------
# C++ nastiness
#---------------------------------------------------------------------------
indent_namespace                = false         # Don't indent name spaces
indent_class                    = false         # Don't indent the class body
indent_extern                   = false         # Don't indent extern 'C'
nl_after_class                  = 2
nl_before_access_spec           = 2
nl_after_access_spec            = 1
indent_access_spec_body         = true
sp_before_class_colon           = force
sp_after_class_colon            = force
sp_func_class_paren             = remove
sp_func_class_paren_empty       = remove
nl_func_class_scope             = remove
nl_func_scope_name              = remove
nl_class_colon                  = remove
sp_before_dc                    = remove
sp_after_dc                     = remove
sp_before_template_paren        = remove        # All of these ensure no spaces in templates
sp_template_angle               = remove
sp_before_angle                 = remove
sp_inside_angle                 = remove
sp_after_angle                  = force
sp_angle_paren                  = remove
sp_angle_paren_empty            = remove
sp_angle_word                   = force
sp_angle_shift                  = remove
sp_permit_cpp11_shift           = true
sp_before_constr_colon          = force
sp_after_constr_colon           = force
nl_constr_colon                 = force
indent_constr_colon             = true
nl_constr_init_args             = force
pos_constr_comma                = trail
pos_constr_colon                = trail
